<?php  //floatval
$string_with_float = "265bitm45.5";
echo floatval($string_with_float);
?>
<br>
<?php  // empty
$var = 0;
$empty_check = empty($var);
var_dump($empty_check);
?>
<br>
<?php  // is_array
$string = "this is an array";
var_dump(is_array($string));
?>
<?php  // is_null
$var = "";
var_dump(is_null($var));
?>
<br>
<?php // isset
var_dump(isset($x));
?>
<br>
<?php // print_r
$var = "this is human readable information";
print_r ($var);
?>
<br>
<?php // unset
$var = "Joe";
unset($var);
var_dump($var);
?>
<br>
<?php // serialize and unserialize
$arr = array("x" , "y" , "z");
$str = serialize($arr);
echo $str;
$previousarr = unserialize($str);
echo "<br>";
print_r($previousarr);
?>
<br>
<?php //gettype
$n = 265;
$type = gettype($n);
var_dump($type);
?>